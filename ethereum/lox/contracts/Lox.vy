from vyper.interfaces import ERC20

implements: ERC20

######################
# Lox Implementation #
######################

struct Deposit:
    value: wei_value
    block_number: uint256

Transfer: event({
    _from: indexed(address),
    _to: indexed(address),
    _value: uint256
})

Approval: event({
    _owner: indexed(address),
    _spender: indexed(address),
    _value: uint256
})

# Used by Lox disbursements
deposits: public(map(address, Deposit))
coinbase_multiplier: public(uint256)

# ERC20-related
name: public(string[64])
symbol: public(string[32])
decimals: public(uint256)
balanceOf: public(map(address, uint256))
allowances: map(address, map(address, uint256))
total_supply: uint256


@public
def __init__(
        _name: string[64],
        _symbol: string[32],
        _coinbase_multiplier: uint256):

    self.name = _name
    self.symbol = _symbol
    self.decimals = 18  # simply match ETH's decimals
    self.total_supply = 0
    self.coinbase_multiplier = _coinbase_multiplier

@public
def disburse():
    """
    @dev Function to disburse owed Lox to the caller,
         based on change in block height, coinbase_multiplier,
         and deposit value. No change in block height or
         zero deposit value will result in a successful
         disbursement of zero tokens.
    """
    deposit: Deposit = self.deposits[msg.sender]
    block_delta: uint256 = block.number - deposit.block_number
    disbursement: uint256 = \
        block_delta \
        * self.coinbase_multiplier \
        * as_unitless_number(deposit.value)
    deposit.block_number = block.number
    self.balanceOf[msg.sender] += disbursement
    self.total_supply += disbursement

@public
@payable
def deposit():
    """
    @dev Function to deposit ETH into the contract.
         Disburses any owed Lox prior to deposit.
    """
    self.disburse()
    self.deposits[msg.sender].value += msg.value

@public
def widthdraw(_value: uint256):
    """
    @dev Function to withdraw ETH from the contract.
         Disburses any owed Lox prior to withdrawal.
    @param _value The amount in wei to withdraw.
    """
    self.disburse()
    self.deposits[msg.sender].value -= _value
    send(msg.sender, _value)


########################
# ERC20 Implementation #
########################

# Based off of:
# https://github.com/ethereum/vyper/blob/76afcb1f599704b748b16272924149a472623a74/examples/tokens/ERC20.vy

@public
@constant
def totalSupply() -> uint256:
    """
    @dev Total number of tokens in existence.
    """
    return self.total_supply

@public
@constant
def allowance(_owner : address, _spender : address) -> uint256:
    """
    @dev Function to check the amount of tokens an owner
         has allowed to a spender.
    @param _owner The address that owns the funds.
    @param _spender The address that may spend the funds.
    @return An uint256 specifying the amount of tokens available
            to be spent by the spender.
    """
    return self.allowances[_owner][_spender]

@public
def transfer(_to : address, _value : uint256) -> bool:
    """
    @dev Transfer token for a specified address
    @param _to The address to transfer to.
    @param _value The amount to be transferred.
    """
    self.balanceOf[msg.sender] -= _value
    self.balanceOf[_to] += _value
    log.Transfer(msg.sender, _to, _value)
    return True

@public
def transferFrom(_from : address, _to : address, _value : uint256) -> bool:
    """
    @dev Transfer tokens from one address to another.
         This function emits a Transfer event.
    @param _from The address that will send the tokens.
    @param _to The address that will receive the tokens.
    @param _value The amount of tokens to be transferred,
                  not honoring decimals.
    @return A bool that is True upon success
    """
    self.balanceOf[_from] -= _value
    self.balanceOf[_to] += _value
    self.allowances[_from][msg.sender] -= _value
    log.Transfer(_from, _to, _value)
    return True


@public
def approve(_spender : address, _value : uint256) -> bool:
    """
    @dev Approve _value for _spender to spend.

         Beware that changing an allowance with this method
         brings the risk that someone may use both the old
         and the new allowance by unfortunate transaction ordering.
         One possible solution to mitigate this race condition
         is to first reduce the spender's allowance to 0 and
         set the desired value afterwards:
         https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729

    @param _spender The address that may spend the funds.
    @param _value The amount of tokens to be spent, not honoring decimals.
    """
    self.allowances[msg.sender][_spender] = _value
    log.Approval(msg.sender, _spender, _value)
    return True
